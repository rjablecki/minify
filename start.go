package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/js"
)

type Config struct {
	JsFiles   []string
	JsOutput  string
	CssFiles  []string
	CssOutput string
}

func dump(param interface{}) {
	fmt.Printf("%T", param)
	fmt.Printf(" (%+v)\n", param)
}

func main() {
	var conf Config

	configFile, e := ioutil.ReadFile("config.json")
	if e != nil {
		fmt.Printf("Can't read config file: %v\n", e)
		os.Exit(1)
	}

	err := json.Unmarshal(configFile, &conf)
	if err != nil {
		fmt.Print("Error:", err)
		os.Exit(1)
	}

	m := minify.New()
	m.AddFunc("text/css", css.Minify)
	m.AddFunc("text/javascript", js.Minify)

	if len(conf.JsFiles) > 0 {
		totalStringJs := ""
		for _, filepath := range conf.JsFiles {
			fileBytes, err := ioutil.ReadFile(filepath)
			if err != nil {
				dump(err)
			}

			fileBytes, err = minify.Bytes(m, "text/javascript", fileBytes)
			if err != nil {
				log.Fatal("minify.Bytes:", err)
			}
			totalStringJs += string(fileBytes)
		}
		err = ioutil.WriteFile(conf.JsOutput, []byte(totalStringJs), 0777)
		if err != nil {
			fmt.Print("Error:", err)
		}
	}

	if len(conf.CssFiles) > 0 {
		totalStringCSS := ""
		for _, filepath := range conf.CssFiles {
			fileBytes, err := ioutil.ReadFile(filepath)
			if err != nil {
				dump(err)
			}
			fileBytes, err = minify.Bytes(m, "text/css", fileBytes)
			if err != nil {
				log.Fatal("minify.Bytes:", err)
			}
			totalStringCSS += string(fileBytes)
		}
		err = ioutil.WriteFile(conf.CssOutput, []byte(totalStringCSS), 0777)
		if err != nil {
			fmt.Print("Error:", err)
		}
	}
	os.Exit(1)
}
