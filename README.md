Minify służy do łącznie i zmniejszania plików javascript oraz css.

# Użycie #

Konfiguracja polega na utworzeniu pliku config.json


```
#!json

{
    "jsFiles": [
        "js/tools.js",
        "js/gogit.js",
        "js/jquery.mCustomScrollbar.js"
    ],
    "jsOutput": "front.min.js",
    "cssFiles": [
        "css/normalize.css",
        "css/font.css",
        "style.css"
    ],
    "cssOutput": "front.min.css"
}
```

